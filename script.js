


const results = document.querySelector('#results');
const filmsLink = 'https://ajax.test-danit.com/api/swapi/films'




function sendRequest(url){
    return fetch(url).then(responce => responce.json())
}

function render(films){
    films.forEach(film => {

        const ul = document.createElement("ul");

        const characters = document.createElement('ul');
        characters.className = 'charactersList';
        characters.innerText = 'characters:'

        for (elem in film) {
            if(elem === 'characters') {
                continue
            }
            const li = document.createElement('li');
            li.innerText = `${elem} : ${film[elem]}`
            ul.append(li);   
        } 

        ul.append(characters);
        results.append(ul);
    })
};




const infoFilm = sendRequest(filmsLink)
.then(films => films.map((film) => {
    return {
    filmTitle: film.name,
    episodeId: film.episodeId,
    opening: film.openingCrawl,
    characters: film.characters,
    }
}))
.then(films => {
    render(films)
    return films.map(film => film.characters)
})
.then(characters => {
    const lists = document.querySelectorAll('.charactersList');
  
    characters.forEach((characterList, index) => {
        const renderedCharacters = document.createElement('ul');
        characterList.forEach(characterLink => {
            sendRequest(characterLink)
            .then(character => character.name)
            .then(name => {
                const li = document.createElement('li');
                li.innerText = name;
                renderedCharacters.append(li);

            })
            
        })
        lists[index].append(renderedCharacters)
    })
});








